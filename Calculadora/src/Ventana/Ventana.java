package ventana;

//Importes

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ventana extends JFrame {

	
	//Creaccion de variables
	JPanel panel;
	JButton boton0, boton1, boton2, boton3, boton4, boton5, boton6, boton7, boton8, boton9;
	JTextField resultado= new JTextField();
	JButton suma, resta, multiplicacion, division, modulo, igual;
	String valoroperacion;
	int r;
	private int a;

	//Constructor
	public Ventana() {
		crearPanel();
		crearBoton();

	}

	private void crearPanel() {
		this.setSize(500, 700);
		this.setTitle("Francisco Guijarro Calculadora");
		this.setLocationRelativeTo(null);
		panel = new JPanel();

		// en esta linea ponemos nosotros los comandos donde queramos

		panel.setLayout(null);

		// Esta linea es parecida al .add
		this.getContentPane().add(panel);
		panel.setBackground(Color.lightGray);
		this.add(panel);

		// Creamos una ventana que es una cuadro
		// Despues a�adimos un panelque es el marco de la foto
		// Y despues metemos la foto dentro del panel
	}

	private void crearBoton() {

		// Pantalla resultado
		resultado.setBounds(16, 16, 448, 100);
		panel.add(resultado);

		// Boton 0
		boton0 = new JButton("0");
		boton0.setBounds(16, 550, 216, 100);
		boton0.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent arg0) {
				String numero=resultado.getText()+boton0.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton0);

		// Boton 1
		boton1 = new JButton("1");
		boton1.setBounds(16, 425, 100, 100);
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton1.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton1);

		// Boton 2
		boton2 = new JButton("2");
		boton2.setBounds(132, 425, 100, 100);
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton2.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton2);

		// Boton 3
		boton3 = new JButton("3");
		boton3.setBounds(248, 425, 100, 100);
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton3.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton3);

		// Boton 4
		boton4 = new JButton("4");
		boton4.setBounds(16, 300, 100, 100);
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton4.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton4);

		// Boton 5
		boton5 = new JButton("5");
		boton5.setBounds(132, 300, 100, 100);
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton5.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton5);

		// Boton 6
		boton6 = new JButton("6");
		boton6.setBounds(248, 300, 100, 100);
		boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton6.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton6);

		// Boton 7
		boton7 = new JButton("7");
		boton7.setBounds(16, 175, 100, 100);
		boton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton7.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton7);

		// Boton 8
		boton8 = new JButton("8");
		boton8.setBounds(132, 175, 100, 100);
		boton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton8.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton8);

		// Boton 9
		boton9 = new JButton("9");
		boton9.setBounds(248, 175, 100, 100);
		boton9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+boton9.getText();
				resultado.setText(numero);
			}
		});
		panel.add(boton9);

		// Boton  mas
		suma = new JButton("+");
		suma.setBounds(364, 550, 100, 100);
		suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+suma.getText();
				resultado.setText(numero);
				valoroperacion="suma";
			}
		});
		
		panel.add(suma);

		// Boton  restar
		resta = new JButton("-");
		resta.setBounds(364, 425, 100, 100);
		resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+resta.getText();
				a=Integer.parseInt(resultado.getText());
				resultado.setText(numero);
				valoroperacion="resta";
			}
		});
		panel.add(resta);

		// Boton  multiplicacion
		multiplicacion = new JButton("X");
		multiplicacion.setBounds(364, 300, 100, 100);
		multiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+multiplicacion.getText();
				a=Integer.parseInt(resultado.getText());
				resultado.setText(numero);
				valoroperacion="multiplicacion";
			}
		});
		panel.add(multiplicacion);

		// Boton division
		division = new JButton("/");
		division.setBounds(364, 175, 100, 100);
		division.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String numero=resultado.getText()+division.getText();
				a=Integer.parseInt(resultado.getText());
				resultado.setText(numero);
				valoroperacion="division";
			}
		});
		panel.add(division);

		// Boton igual
		igual = new JButton("=");
		igual.setBounds(248, 550, 100, 100);
		igual.addActionListener(new ActionListener() {
			
			//Menu operaciones (estan incompletas)
			public void actionPerformed(ActionEvent arg0) {
				switch(valoroperacion) {
				
				//Suma
				case "suma": 
					
					break;
					
				//Resta
				case "resta":
					
					break;
				case "multiplicacion":
					
					break;
				case "division":
				
					break;
				}
				resultado.setText(String.valueOf(r));
				valoroperacion="";


			}
		});
		panel.add(igual);

	}
}