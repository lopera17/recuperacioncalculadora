package ventana;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPrincipal {
	int a,b;
	operaciones op=new operaciones(a, b);

	@Test
	public void sumatest() {

		int z=op.suma(3, 2);
		assertEquals(5, op.suma(2, 3), 0);
		assertEquals(z, op.suma(3, 2), 0);
	}
	
	@Test
	public void restatest() {
		int z=op.resta(5, 3);
		assertEquals(2, z, 0);
		assertEquals(-2, op.resta(3, 5), 0);
	}
	
	@Test
	public void multtest() {
		int z=op.multiplicacion(2, 4);
		assertEquals(8, z, 0);
		assertEquals(8, op.multiplicacion(4, 2), 0);
		assertEquals(0, op.multiplicacion(4, 0), 0);
	}
	
	@Test
	public void divtest() {
		double z=op.division(10, 2);
		assertEquals(5, z, 0);
		assertEquals(0.5, op.division(1, 2), 0);
		assertEquals(0, op.division(4, 0), 0);
	}
}